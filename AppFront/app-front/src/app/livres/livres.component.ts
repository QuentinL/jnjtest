import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-livres',
  templateUrl: './livres.component.html',
  styleUrls: ['./livres.component.css']
})
export class LivresComponent implements OnInit {

  @Input() livreName: string;
  @Input() livreRef: string;

  constructor() { }

  ngOnInit(): void {
  }
  
}
