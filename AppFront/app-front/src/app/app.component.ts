import { Component, OnInit } from '@angular/core';
import { LivreService } from './services/livre.service';
import { HttpClient } from '@angular/common/http';
import { Livre } from './models/livre';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
   private env = environment;
   public livre: Livre[];
  
   constructor(private httpClient: HttpClient, livreService: LivreService) {
	   this.getLivres();
   }
  
  ngOnInit(){
	  
  }
  
 /**
   * Récupération des Livres via un appel vers le serveur Node
   */
  getLivres() {
   this.httpClient.get<Livre[]>(this.env.urlNodeServer + '/livres')
      .subscribe(
        (response) => {
          console.log(response);
          this.livre = response;
          console.log(this.livre);
        },
        (error) => {
          console.log(error);
        }
      )
	 
  }
}
