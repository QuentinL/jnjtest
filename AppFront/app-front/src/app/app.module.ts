import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LivresComponent } from './livres/livres.component';
import { LivreService } from './services/livre.service';


const appRoot: Routes = [
	{ path: '',  component: AppComponent },
	{ path: 'list-livres',  component: AppComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LivresComponent
  ],
  imports: [
    BrowserModule,
	FormsModule,
	HttpClientModule,
	ReactiveFormsModule
  ],
  providers: [
	LivreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
