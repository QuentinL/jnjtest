import { User } from './user';

/**
 * Model front pour l'objet Livre
 */
export class Livre {
    public id: number;
    public nom: string;
    public reference: string;
	public userId : number;
    //public user: User;
}