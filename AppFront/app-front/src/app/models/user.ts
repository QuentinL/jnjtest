/**
 * Model front pour l'objet User
 */
export class User {
    public id: number;
    public nom: string;
    public prenom: string;
	public login: string;
	public password: string;
    
}