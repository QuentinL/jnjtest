import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Livre } from '../models/livre';

@Injectable({
  providedIn: 'root'
})
export class LivreService {
	private env = environment;
	 private livre: Livre[];
	 
	

  constructor(private httpClient: HttpClient) { }
  
  /**
   * Récupération des Formations via un appel vers le serveur Node
   */
  getLivres() {
    this.httpClient.get<Livre[]>(this.env.urlNodeServer + '/livres')
      .subscribe(
        (response) => {
          console.log(response);
          this.livre = response;
          console.log(this.livre);
        },
        (error) => {
          console.log(error);
        }
      )
  }
}


