//Imports
const express = require('express');                             //module server           
const bodyParser = require('body-parser');                      //parser en json
const cors = require('cors');                                   //mise en place de régles d'access
const request = require('request');           					//module http pour communiquer avec .net
var async = require('async');                                   //module async pour les appels asyncrones

//initialiser les cors
var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200,
    target: 'http://127.0.0.1:6969'
}

//le server
const app = express();                                          // le server
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors(corsOptions));

var urlNet = 'https://localhost:5001';                       //a changer en function 
var port = parseInt(process.env.PORT) || 6969;

/*===================== ROUTE Livre ============================================================================*/


app.route('/livres').post((req, res, next) => {
    request.post(urlNet + '/api/Livres', { json: req.body }, (err, response, body) => {
        if (err) {
            console.log(err.toJSON);
        } else if (response.statusCode != 200) {
            //console.log("287 " + response.body['message']);
            res.sendStatus(response.statusCode).send(response.message);
            res.end();
            next();
        }
        else {
            //console.log(' 293 : ' + response.body['message']);
        }
    });
}).patch((req, res, next) => {
    async.series([
        function (callback) {
            request.patch(urlNet + '/api/Livres/5' + req.body.id, { json: req.body }, (err, resp, body) => {
                if (err) {
                    console.log(err.toJSON);
                    next();
                } else {
                    res.status(resp.statusCode).send(body).end();
                    next();
                }
            });
        }
    ])
}).get((req, res, next) => {
    request.get(urlNet + '/api/Livres', (err, resp, body) => {
        if (err) {
            console.log("erreur : "+err.toJSON);
            next();
        }
        else if (resp.statusCode === 200) {
            res.send(resp.body);
            res.end();
            next();
        }
    })
})
/***
 * get du livre
 */

app.route('/livre/:id').get((req, res, next) => {
    request.get(urlNet + '/api/Livres/' + req.params.id, (err, resp, body) => {

        if (err) {
            console.log(err.toJSON());
            next();
        }
        else if (resp.statusCode === 200) {
            res.send(resp.body);
            res.end();
            next();
        }
    })
})

/*===================== FIN ROUTE LIVRE ========================================================*/

/*===================== ROUTE USER ============================================================================*/


app.route('/users').post((req, res, next) => {
    //console.log("formation : ", JSON.stringify(req.body));
    request.post(urlNet + '/api/Users', { json: req.body }, (err, response, body) => {
        if (err) {
            console.log(err.toJSON);
        } else if (response.statusCode != 200) {
            //console.log("287 " + response.body['message']);
            res.sendStatus(response.statusCode).send(response.message);
            res.end();
            next();
        }
        else {
            //console.log(' 293 : ' + response.body['message']);
        }
    });
}).patch((req, res, next) => {
    async.series([
        function (callback) {
            request.patch(urlNet + '/api/Users/5' + req.body.id, { json: req.body }, (err, resp, body) => {
                if (err) {
                    console.log(err.toJSON);
                    next();
                } else {
                    res.status(resp.statusCode).send(body).end();
                    next();
                }
            });
        }
    ])
}).get((req, res, next) => {
    request.get(urlNet + '/api/Users', (err, resp, body) => {
        if (err) {
            console.log(err.toJSON);
            next();
        }
        else if (resp.statusCode === 200) {
            res.send(resp.body);
            res.end();
            next();
        }
    })
})
/***
 * get d'un utilisateur
 */

app.route('/user/:id').get((req, res, next) => {
    request.get(urlNet + '/api/Users/5' + req.params.id, (err, resp, body) => {

        if (err) {
            console.log(err.toJSON());
            next();
        }
        else if (resp.statusCode === 200) {
            res.send(resp.body);
            res.end();
            next();
        }
    })
})

/*===================== FIN ROUTE USER ========================================================*/

// démarrage server
app.listen(port, () => {
    console.log('Server started on port ' + port);
})