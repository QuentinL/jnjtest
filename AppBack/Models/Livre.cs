﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppBack.Models
{
    public class Livre
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Reference { get; set; }
        public int UserId { get; set; }
        //récupération de l'utilisateur
        public User User { get; set; }
    }
}
