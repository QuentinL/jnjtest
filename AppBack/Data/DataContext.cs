﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppBack.Models;

namespace AppBack.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        // à répéter pour chaque model
        public DbSet<User> Users { get; set; }
        public DbSet<Livre> Livres { get; set; }
    }
}
